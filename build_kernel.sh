#!/bin/bash

# Execute it in source directory of kernel.

Model=SM-G977P
export ARCH=arm64
export PATH=/mnt/ToolChain/aarch64-linux-android-4.9/bin:/mnt/ToolChain/llvm-arm-toolchain-ship/6.0/bin:$PATH
export KERNEL_MAKE_ENV="DTC_EXT=$(pwd)/tools/dtc CONFIG_BUILD_ARM64_DT_OVERLAY=y"
export KBUILD_BUILD_VERSION="Custom S10 Kernel For $Model"
outDir=$(pwd)/../Out_$(date '+%d_%m_%Y')
mkdir -p $outDir
echo "Out directory: $outDir"

BUILD_CROSS_COMPILE=aarch64-linux-android-
KERNEL_LLVM_BIN=/mnt/ToolChain/llvm-arm-toolchain-ship/6.0/bin/clang
CLANG_TRIPLE=aarch64-linux-gnu-
CONFIG_FILE=beyondxq_usa_spr_defconfig

make -j8 -C $(pwd) O=$outDir CC=clang $KERNEL_MAKE_ENV ARCH=arm64 CROSS_COMPILE=$BUILD_CROSS_COMPILE REAL_CC=$KERNEL_LLVM_BIN CLANG_TRIPLE=$CLANG_TRIPLE $CONFIG_FILE
make -j8 -C $(pwd) O=$outDir CC=clang $KERNEL_MAKE_ENV ARCH=arm64 CROSS_COMPILE=$BUILD_CROSS_COMPILE REAL_CC=$KERNEL_LLVM_BIN CLANG_TRIPLE=$CLANG_TRIPLE

echo "Output files"
echo "  - Kernel : arch/arm64/boot/Image"
echo "  - module : drivers/*/*.ko"
